%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function [idm,idw] = getGuessCandidates(WK,pend,current_idx,params)

idw = getNeighbors(WK,pend,params,1.43);
idw = idw((idw~=current_idx)==1);
idw = idw((idw~=current_idx)==1);
idx_wk =(WK(idw,4)==1);
idm = idw(idx_wk==1);

end
