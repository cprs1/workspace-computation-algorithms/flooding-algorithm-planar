%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function grid = defineGrid(params)
boxsize = params.boxsize;
    stepsize = params.stepsize;
    
    xL = boxsize(1);
    xR = boxsize(2);
    yU = boxsize(3);
    yB = boxsize(4);
    n_sampX = floor(abs(xL-xR)/stepsize);
    n_sampY = floor(abs(yU-yB)/stepsize);
    x = linspace(xL,xR,n_sampX);
    yg = linspace(yB,yU,n_sampY);
    [X,Y] = meshgrid(x,yg);
    Xp = reshape(X,n_sampX*n_sampY,1);
    Yp = reshape(Y,n_sampX*n_sampY,1);
    grid = [Xp,Yp];

end