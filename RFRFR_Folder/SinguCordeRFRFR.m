function [flag1,flag2] = SinguCordeRFRFR(jac)
% jac = full jacobian matrix of IGSP problem
Z = null(jac(end-5:end-2,3:end-4)); % null space of gradient of constraints

% first group of matrices
Jth1 = jac(1:end-6,3:end-6);
Ja1 =  jac(1:end-6,1:2);
Jp1 =  jac(1:end-6,end-5:end-4);

% second group of matrices
Jth2 = jac(end-5:end-2,3:end-6);
Ja2 =  jac(end-5:end-2,1:2);
Jp2 =  jac(end-5:end-2,end-5:end-4);

% collect
Jth = [Z'*Jth1;Jth2];
Jp = [Z'*Jp1;Jp2];
Ja = [Z'*Ja1;Ja2];

% T1 e T2 matrices
typeI  = [Jth,Ja];
typeII = [Jth,Jp];

% evaluate inverse cond number: when is 0 --> singular
flag1 = rcond(typeI);
flag2 = rcond(typeII);
end