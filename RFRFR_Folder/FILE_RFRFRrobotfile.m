%% ROBOT PARAMETERS RFRFR

%% Robot Geometry Parameters

pA1 = [-0.2;0]; % motor position 1 [m]
pA2 = [+0.2;0]; % motor position 2 [m]
L = 1; % links lenght
th1lim = [-Inf,Inf]; % motor 1 angular limits [rad] (set -Inf Inf to neglect)
th2lim = [-Inf,Inf]; % motor 2 angular limits [rad] (set -Inf Inf to neglect)

%% Robot Material Parameters

r = 0.001; % cross section radius [m]
E = 210*10^9; % young modulus [Pa]
A = pi*r^2; % cross section area [m^2]
I = 0.25*pi*r^4; % cross section inertia moment [m^4]
rho = 7800; % density [kg/m^3]

EI = E*I; 

stresslim = Inf; % Maximum puntual stress [Pa] (set Inf to neglect)

%% External loads

g = [0;9.81]; % gravity (set 0 to neglect)
fd = rho*A*g; % distributed loads in global frame
fend = [0;1.5]; % end effector force in global frame

%% Model Parameters

Nelt = 50; % N. of elements for each link
Le = L/Nelt; % element lenght
k = EI/Le; % equivalent torsional stiffness

inflA = +1; % sign of "first attempt" curvature first beam
inflB = -1; % sign of "first attempt" curvature second beam

pstart = [0;0.8]; % start position
params.pstart = pstart; %store

%% Simulation functions

% Objective function for IGSP problem
fcn.objetivefcn = @(y,pend) CordeIGSPeqnRFRFR(y,k,Le,fend,fd,pend,pA1,pA2);

% Mechanical constraint evaluation function
fcn.mechconstrfcn = @(y) mechconstrRFRFR(y,th1lim,th2lim,pA1,pA2,L,r,E,stresslim,Nelt);

% Singularity evaluation funciton
fcn.singufcn = @(jac) SinguCordeRFRFR(jac);

% Stability evaluation function
fcn.stabilityfcn = @(jac) StabilityCordeRFRFR(jac);

% Robot internal energy function
fcn.internfcn = @(y) internalEnergyRFRFR(y,k);

%% Solution of the first IGSP

output = CCInverseRFRFRGuess(pA1,pA2,pstart,L,Nelt,inflA,inflB); % constant curvature approximation
alpha1 = output(3,:)';
alpha2 = output(9,:)';
y0 = [alpha1(1);alpha2(1);alpha1(2:end);alpha2(2:end);pstart;zeros(4,1)]; % constant curvature initial guess
fun = @(y) CordeIGSPeqnRFRFR(y,k,Le,fend,fd,pstart,pA1,pA2); 
sol = fsolve(fun,y0); % solution

params.y0 = sol; % store
[pos1,pos2] = posRFRFRCorde(sol,Nelt,Le,pA1,pA2); % function for links reconstruction
PlotRFRFR(pos1,pos2,L); % robot plot function
axis equal
axis([-1 1 -1 1])
title('Initial Guess Configuration')
drawnow % to draw immediately
