function V = internalEnergyRFRFR(y,k)
n = max(size(y));
Nel = (n-6)/2;
Nth = Nel-1;

% Extract Variables
qA1 = y(1,1);
qA2 = y(2,1);
th = y(3:end-4,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
alpha1 = [qA1;th1];
alpha2 = [qA2;th2];

% internal energy of each equivalent torsional spring
idx = 2:Nel;
d1 = alpha1(idx)-alpha1(idx-1);
d2 = alpha2(idx)-alpha2(idx-1);
d1q = d1.^2;
d2q = d2.^2;

V = 0.5*k*(sum(d1q)+sum(d2q)); % internal energy of the robot

end