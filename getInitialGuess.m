%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function y0 = getInitialGuess(WK,guesses,idmentry)
[~,idmin] = min(WK(idmentry,9));
idm = idmentry(idmin);
y0 = guesses(:,idm);
end