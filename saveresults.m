%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function WK_line = saveresults(y,jac,fcn)
Stability = fcn.stabilityfcn;
Singu = fcn.singufcn;
InternalEnergy = fcn.internfcn;

id_7 = Stability(jac); 
[id_8,id_9] = Singu(jac);
id_10 = norm(inv(jac),Inf); 
id_11 = InternalEnergy(y);
id_5_6 = [1,1];
                
WK_line = [id_5_6,id_7,id_8,id_9,id_10,id_11];

end