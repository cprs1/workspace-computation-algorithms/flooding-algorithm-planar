%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function id2 = getNeighbors(WK,pstart,params,fact)
stepsize = params.stepsize;
id = ((abs(WK(:,2)-pstart(1))<=fact*stepsize) & (abs(WK(:,3)-pstart(2))<=fact*stepsize));
id2 = WK(id==1,1);
end