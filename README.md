# Flooding Algorithm - Planar

Flooding algorithm for the computation of spatial workspaces of CPRs
 
Last update by F.Zaccaria on 02 February 2022

Algorithm from the paper "Briot, S., & Goldsztejn, A. (2021). Singularity Conditions for Continuum Parallel Robots. IEEE Transactions on Robotics."
