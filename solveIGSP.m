%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function [y,jac,flag] = solveIGSP(fcn,params,pend,y0)

Cordefun = fcn.objetivefcn;
feq = @(y) Cordefun(y,pend);
solverflag = params.solver;
maxiter = params.maxiter;

switch solverflag
    case 'Newton'
        TOL = params.TOL;
        prec = params.prec;
        [y,~,jac,flag,~] = Newton(feq,y0,maxiter,TOL,prec);
    case 'fsolve'
        options = optimoptions('fsolve','Display','off','Maxiter',maxiter','Algorithm','trust-region','SpecifyObjectiveGradient',true,'CheckGradients',false);
        [y,~,flag,~,jac] = fsolve(feq,y0,options);
        jac = full(jac);
end
        
%%
% ----------------------
% Auxiliary function Newton Solver
    function [y,fun,jac,flag,iter] = Newton(eq,y0,maxiter,TOL,prec)
        iter = 0;
        err = 1;
        [fun,jac] = feval(eq,y0);
        while (err>TOL && iter<=maxiter)
            y = y0 - (prec*jac) \ (prec*fun);
            iter = iter + 1;
            [fun,jac] = feval(eq,y);
            err = max(norm(y-y0),norm(fun));
            y0 = y;
        end
        flag = (iter<=maxiter);
    end
end