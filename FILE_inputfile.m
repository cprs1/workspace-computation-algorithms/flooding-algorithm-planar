%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

%% INPUT FILE

%% Simulation parameters

maxiter = 10;
TOL = 10^-6;
TOL2 = 1200;
stepsize = 0.01;
boxsize = [-1 1  -1 1 ];



%% STORE VARIABLES

params.stepsize = stepsize;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.solver = 'fsolve';


