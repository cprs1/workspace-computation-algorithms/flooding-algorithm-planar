%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function [WK,outstruct] = FCN_FloodingPlanarWK(fcn,params,instantplot)
tic;

%% COLLECT INPUTS
y0 = params.y0;
pstart = params.pstart;
stepsize = params.stepsize;
TOL2 = params.TOL2;
mech = fcn.mechconstrfcn; 

%% DEFINE GRID
table = defineGrid(params);
np = numel(table(:,1));
idx = 1:1:np;
WK = [idx',table,zeros(np,7)];
guesses = zeros(max(size(y0)),np);

%% SOLVE FIRST ITERATION
id2 = getNeighbors(WK,pstart,params,1.43);
pend = [WK(id2(1),2);WK(id2(1),3)];
[y,jac,~] = solveIGSP(fcn,params,pend,y0);
% store results
WK(id2(1),4:10) = saveresults(y,jac,fcn);
guesses(:,id2(1)) = y;

% initialize
idn2 = getNeighbors(WK,pend,params,1.43);
idn2 = idn2((idn2~=id2(1))==1);
wk_todo = idn2;
wk_todoend = [];

%% MAIN LOOP
while (numel(wk_todo)>0 || numel(wk_todoend)>0)
    if (numel(wk_todo)==0)
        wk_todo = wk_todoend;
        wk_todoend = [];
    end
    
    %% EXTRACT POINT
    current_idx = wk_todo(1);
    WK(current_idx,5) = 1;
    wk_todo = wk_todo(2:end);
    pend = [WK(current_idx,2);WK(current_idx,3)];
    
    %% CHOOSE GUESS CANDIDATES
    [idmentry,idw] = getGuessCandidates(WK,pend,current_idx,params);
    
    %% CHOOSE BEST INITIAL GUESS      
    y0 = getInitialGuess(WK,guesses,idmentry);
    
    %% COMPUTATION
    [y,jac,flag] = solveIGSP(fcn,params,pend,y0);
    flags = (flag & norm(inv(jac),Inf)<=TOL2 & mech(y));
    WK(current_idx,4:10) = saveresults(y,jac,fcn);
    % if ok: not T1 singu and feasible
    if (flags==1)
        guesses(:,current_idx) = y;
        [wk_todo, wk_todoend ,idny2] = toDoManager(WK,params,idw,WK(current_idx,8),wk_todo,wk_todoend);
        WK(idny2,5) = 1;
        if instantplot == 1
            plot(pend(1),pend(2),'b.')
            drawnow;
        end
     % if T1 singu or not feasible
    else
        WK(current_idx,4) = 3;
        if (flag==1 && (norm(inv(jac),Inf)<=TOL2)==1 && mech(y)==0)
            WK(current_idx,4) = 2;
        end
        if instantplot ==1
           plot(pend(1),pend(2),'r.')
           drawnow;
        end
    end

end

%% DATA
time = toc;
wk_idx = WK(WK(:,4)==1,1);
unst_idx = WK((WK(:,6)~=0 & (WK(:,9)~=0)),1);
wk_computed = WK(WK(:,9)~=0,1);
outstruct.time = time/60;
outstruct.computedpoints = numel(wk_computed);
outstruct.stablevolume = numel(WK(wk_idx),1)*stepsize.^3;
outstruct.unstablvolume = numel(WK(unst_idx),1)*stepsize.^3;

end