%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function FCN_plot(WK,parameters)

TOL = parameters.TOL;
TOL2 = parameters.TOL2;
boxsize = parameters.boxsize;

idwk = (WK(:,4) == 1 & WK(:,6) == 0);
idun = (WK(:,4) == 1 & WK(:,6) ~= 0);
idT1 = (WK(:,9)>TOL2);
idT2 = (WK(:,8)<TOL & WK(:,9)>0);
idlimits = (WK(:,4) == 2);

% figure()
plot(WK(idwk,2),WK(idwk,3),'b.')
hold on
plot(WK(idun,2),WK(idun,3),'y.')
plot(WK(idT2,2),WK(idT2,3),'k.')
plot(WK(idT1,2),WK(idT1,3),'r.')
if numel(idlimits)>0
    plot(WK(idlimits,2),WK(idlimits,3),'.','Color',[0, 0.5, 0])
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace')

end