%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

%% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
FILE_RFRFRrobotfile

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 1;
[WK,outstruct] = FCN_FloodingPlanarWK(fcn,params,instantplot);

%%

FCN_plot(WK,params)
