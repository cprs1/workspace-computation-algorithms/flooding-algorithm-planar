%% PLANAR FLOODING ALGORITHM
% F.Zaccaria 02 February 2022
% algorithm from the paper "Singulariy Conditions of Continuum Parallel
% Robots"

function [wk_todo, wk_todoend ,idny2] = toDoManager(WK,params,idw,singuval,wk_todo,wk_todoend)
TOL = params.TOL;

if singuval>=TOL
    idny2 = idw(WK(idw,5)==0 & WK(idw,6)==0);
    wk_todo = [wk_todo;idny2];
else
    idny2 = idw(WK(idw,5)==0 & WK(idw,6)==0);
    wk_todoend = [wk_todoend;idny2];
end
end